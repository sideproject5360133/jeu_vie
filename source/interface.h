
#include "global.h"

#include <SDL/SDL.h>
#include <SDL/SDL_keysym.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

/** Constante utilisé pour gérer l'affichage **/

#define WINHI 700 // window height
#define WINWI 1300 // window width
#define RECWI 240 //longueur d'un rectangle
#define RECHI 35 //Hauteur d'un rectangle

#define POSX_NOUVELLE_PARTIE 20 //Menu principal
#define POSY_NOUVELLE_PARTIE 80
#define POSX_JOUER 20
#define POSY_JOUER 120
#define POSX_CHARGER 20
#define POSY_CHARGER 160
#define POSX_QUITTER 20
#define POSY_QUITTER 650
#define POSX_INFO 20
#define POSY_INFO 500

#define POSX_PLAY 20
#define POSY_PLAY 80
#define POSX_VIT 20
#define POSY_VIT 120
#define POSX_CAD 20
#define POSY_CAD 160
#define POSX_EFFACER 20
#define POSY_EFFACER 200
#define POSX_GRILLE 20
#define POSY_GRILLE 240
#define POSX_EFFAC 20
#define POSY_EFFAC 470


#define POSX_HVSH 20 //Menu de jeu
#define POSY_HVSH 80
#define POSX_HVSIA1 20
#define POSY_HVSIA1 120
#define POSX_HVSIA2 20
#define POSY_HVSIA2 160
#define POSX_IAVSIA 20
#define POSY_IAVSIA 200
#define POSX_RETOUR 20
#define POSY_RETOUR 240

#define POSX_CHARGER_1 20 //chargement
#define POSY_CHARGER_1 80
#define POSX_CHARGER_2 20
#define POSY_CHARGER_2 120
#define POSX_CHARGER_3 20
#define POSY_CHARGER_3 160
#define POSX_CHARGER_4 20
#define POSY_CHARGER_4 200
#define POSX_CONFIRMER 20
#define POSY_CONFIRMER 260
#define POSX_RETOUR_CHAR 20
#define POSY_RETOUR_CHAR 300

#define POSX_SAUVER 20 //En jeu
#define POSY_SAUVER 80
#define POSX_UNDO 20
#define POSY_UNDO 120
#define POSX_HISTORIQUE 20
#define POSY_HISTORIQUE 160
#define POSX_RETOUR_JEU 20
#define POSY_RETROU_JEU 200
#define POSX_JOUER_IA 20
#define POSY_JOUER_IA 280
#define WIDT_JOUER_IA 240
#define HIGH_JOUER_IA 150
#define POSX_MINI_TOUR 400
#define POSY_MINI_TOUR 460
#define POSX_MINI_HISTORIQUE 400
#define POSY_MINI_HISTORIQUE 500 

#define POSX_SAUVER_1 20 //sauvegarde
#define POSY_SAUVER_1 80
#define POSX_SAUVER_2 20
#define POSY_SAUVER_2 120
#define POSX_SAUVER_3 20
#define POSY_SAUVER_3 160
#define POSX_SAUVER_4 20
#define POSY_SAUVER_4 200

#define POSX_CONFIRMER_ALERTE 100 //Position alerte
#define POSY_CONFIRMER_ALERTE 350
#define POSX_ANNULER_ALERTE 400
#define POSY_ANNULER_ALERTE 350
#define POSX_IA1VSIA1 25
#define POSY_IAvsIA 350
#define POSX_IA1VSIA2 275
#define POSX_IA2VSIA2 525

#define POSX_PLATEAU 275 //position du plateau
#define POSY_PLATEAU 75
#define PLATEAUWI 524
#define PLATEAUHI 319

#define DEPART_PLATEAU_X 18 //détection plateau
#define DEPART_PLATEAU_Y 18
#define HORIZONTAL_Y 8
#define TAILLE_H 25

#define DEPART_PLATEAU_X_A 18  //affichage plateau
#define DEPART_PLATEAU_Y_A 24
#define HORIZONTAL_Y_A 8
#define TAILLE_H_A 26

/* Variable globale utilisé pour l'interface */
SDL_Surface *ecran, *texte, *rectangle, *big_rectangle, *ia_rectangle;
Uint32 vert,rouge,bleu,gris;
SDL_Surface *pion,*pionEffac,*pionCentre;
TTF_Font *fontMenu;
SDL_Rect posTexte,posRect,posPion;


//Initialisation du SDL pour l'affichage
void init_SDL_global();

//Procédure d'effaçage de l'écran
void effacer();

/*Affichage du plateau de jeu
 * Entrée: plateau de jeu */
void affichage_plateau(int plateau[TAILLE_X][TAILLE_Y]);

void affichage_grille(int plateau[11][11]);


void rajout_pion_grille(int x,int y);
//Rajout d'un pion sur le plateau
void rajout_pion_grille_eff(int x,int y);

/* Affichage d'un rectangle avec du texte
 * Entrée: -chaine de caractère (texte dans le rectangle)
 * 		   -entier x = coordonnées x du rectangle
 * 		   -entier y = coordonnées y du rectangle */
void affichage_rectangle(char ch[],int x,int y);

/* Affichage d'un texte (sans rectangle)
 * Entrée: -chaine de caractère (texte)
 * 		   -entier x = coordonnées x du texte
 * 		   -entier y = coordonnées y du texte */
void affichage_texte(char ch[],int x,int y);

/* Affichage du mini historique
 * Entrée: -entier j = numéro du joueur
 * 		   -entier l = ligne de l'historique à afficher
 * 		   -tableau d'entier hist = historique de jeu
 * 		   -booléen fin qui indique si le jeu est fini ou non */
void affichage_mini_historique(int j,int l,int hist[3][130],int fin);

/* Affichage du menu principal
 * Entrée: plateau de jeu */
void affichage_menu_principal(int plateau[11][11]);

/* Affichage du menu de jeu
 * Entrée: plateau de jeu */
void affichage_menu_jeu(int plateau[11][11]);

/* Affichage du menu de chargement
 * Entrée: -plateau de jeu
 * 		   -booléen confirmer (affichage du bouton confirmer ou non)*/
void affichage_menu_chargement(int plateau[11][11],int confirmer);

/* Affichage du menu en jeu
 * Entrée: -plateau de jeu
		   -booléen IA (il y a une IA ou non) */
void affichage_menu_en_jeu(int plateau[11][11],int IA);

/* Affichage du menu de sauvegarde
 * Entrée: -plateau de jeu
 * 		   -booléen confirmer (affichage du bouton confirmer ou non) */
void affichage_menu_sauv(int plateau[11][11],int confirmer);

/* Affichage des infos */
void affiche_info();

/* Affichage d'une alerte
 * Entrée: -chaine de caractère ch = question
 * 		   -chaine de caractère ch2 = choix n°1
 * 		   -chaine de caractère ch3 = choix n°2 */
void affichage_alerte(char ch[],char ch2[],char ch3[]);

void affichage_menu(int play,int vit,int cad);

/* Affichage de l'historique
 * Entrée: -Tableau d'entier = historique de jeu
 * 		   -entier length = longueur de l'historique */
void affichage_historique(int hist[3][130],int length);

/* Rajout d'un pion sur l'affichage
 * Entrée: -entier x = coordonnées x du pion
 * 		   -entier y = coordonnées y du pion
 * 		   -entier y = couleur du pion */
void rajout_pion(int x,int y);

/* Reconnaissance d'un clic
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * 		   -entier x2 = coordonnées x du rectangle
 * 		   -entier y2 = coordonnées y du rectangle
 * Sortie: -Renvoie un booléen:
 * 				1 si le clic appartient au rectangle
 * 				0 si le clic n'appartient pas au rectangle */
int reco_clic(int x,int y,int x2,int y2);

/* Reconnaissance d'un clic
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * 		   -entier x2 = coordonnées x du début du rectangle
 * 		   -entier y2 = coordonnées y du début du rectangle
 * 		   -entier x3 = coordonnées x de la fin du rectangle
 * 		   -entier y3 = coordonnées y de la fin du rectangle
 * Sortie: -Renvoie un booléen:
 * 				1 si le clic appartient au rectangle
 * 				0 si le clic n'appartient pas au rectangle */
int reco_clic_complet(int x,int y,int x2, int y2, int x3, int y3);

/* Gestion du menu principal
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * Sortie: Renvoie:
 * 			-0 si rien détecter
 * 			-1 si "Jouer"
 * 			-2 si "Nouvelle partie"
 * 			-3 si "Charger"
 * 			-4 si "Quitter"
 * 			-5 si "Infos diverses" */
int menu_principal(int x,int y);

/* Gestion du menu de jeu
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * Sortie: Renvoie:
 * 			-0 si rien detecter
 * 			-1 si H vs H
 * 			-2 si H vs IA1
 * 			-3 si H vs IA2
 * 			-4 si Retour
 * 			-5 si IA vs IA */
int menu_jeu(int x,int y);

/* Gestion du menu de chargement
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * 		   -booléen confirmer (affichage du bouton confirmer ou non)
 * Sortie: Renvoie:
 * 			-0 si rien detecter
 * 			-1 si Sauvegarde A
 * 			-2 si Sauvegarde B
 * 			-3 si Sauvegarde C
 * 			-4 si Sauvegarde D
 * 			-5 si Confirmer
 * 			-6 si retour */
int menu_chargement(int x,int y,int *confirmer);

/* Gestion du menu en jeu
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * 		   -booléen IA (affichage du bouton d'IA ou non)
 * Sortie: Renvoie:
 * 			-0 si rien detecter
 * 			-1 si Sauvegarde
 * 			-2 si Undo
 * 			-3 si Historique
 * 			-4 si retour
 * 			-5 si clic sur le plateau de jeu
 * 			-6 si faire jouer l'IA */
int menu_en_jeu(int x,int y,int IA);

/* Gestion du menu IA contre IA
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * Sortie: Renvoie:
 * 			-0 si rien detecter
 * 			-1 si IA1 vs IA1
 * 			-2 si IA1 vs IA2
 * 			-3 si IA2 vs IA2 */
int menu_IAvsIA(int clicX,int clicY);

/* Gestion du menu de sauvegarde
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * 			-booléen confirmer (affichage ou non)
 * Sortie: Renvoie:
 * 			-0 si rien detecter
 * 			-1 si Sauvegarde A
 * 			-2 si Sauvegarde B
 * 			-3 si Sauvegarde C
 * 			-4 si Sauvegarde D
 * 			-5 si confirmer
 * 			-6 si retour */
int menu_sauvegarde(int x,int y,int *confirmer);

/* Gestion d'une "alerte"
 * Entrée: -entier x = coordonnées x du clic
 * 		   -entier y = coordonnées y du clic
 * Sortie: Renvoie:
 * 			-0 si rien detecter
 * 			-1 si premier choix
 * 			-2 si second choix*/
int alerte(int x,int y);
