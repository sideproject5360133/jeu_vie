

/**********************************************************************/
/** Spécification des procédures et fonctions données dans le header **/
/**********************************************************************/

#include "interface.h"

SDL_Color fonteNoire={0,0,0};

//initialisation de variables globales
//code partiellement repris du code disponible sous moodle
void init_SDL_global()
{
	/* bout de code repris du code donnée sous moodle */
	Uint32 initflags = SDL_INIT_VIDEO; //| SDL_DOUBLEBUF;  /* See documentation for details */
    Uint8  video_bpp = 32; // 32 bits de couleur
    Uint32 videoflags = SDL_HWSURFACE; // utiliser la mémoire vidéo
	 /* Initialize the SDL library */
    if ( SDL_Init(initflags) < 0 )
    {
        fprintf(stderr, "N'arrive pas a` initialiser la SDL : %s\n", SDL_GetError());
        exit(1);
    }
    /* Set video mode */
    ecran = SDL_SetVideoMode(WINWI, WINHI, video_bpp, videoflags);
    if (ecran == NULL)
    {
        fprintf(stderr, "N'arrive pas a` etablir mode video%dx%dx%d : %s\n", WINWI, WINHI, video_bpp, SDL_GetError());
        SDL_Quit();
        exit(2);
    }
	TTF_Init();
    fontMenu = TTF_OpenFont("CAC-Champagne/PTC55F.ttf",21);
    vert = SDL_MapRGB(ecran->format,0,128,0); // c'est du vert
    rouge = SDL_MapRGB(ecran->format,255,0,0); // c'est du rouge
    bleu = SDL_MapRGB(ecran->format,60,50,255); // c'est du bleu
	gris = SDL_MapRGB(ecran->format,75,75,75); // c'est du gris
	rectangle = SDL_CreateRGBSurface(initflags,RECWI,RECHI,video_bpp,0,0,0,0);
	big_rectangle = SDL_CreateRGBSurface(initflags,PLATEAUWI-150,PLATEAUHI-170,video_bpp,0,0,0,0);
	ia_rectangle= SDL_CreateRGBSurface(initflags,WIDT_JOUER_IA,HIGH_JOUER_IA,video_bpp,0,0,0,0);
    SDL_WM_SetCaption("Jeu de Hex", NULL); // legende de la fenêtre
    SDL_FillRect(rectangle,NULL,bleu);
    SDL_FillRect(big_rectangle,NULL,rouge);
    SDL_FillRect(ia_rectangle,NULL,gris);
    pion = IMG_Load("Images/pion.png");
    pionEffac = IMG_Load("Images/pion_effac.png");
    pionCentre = IMG_Load("Images/croix.png");
}

//Effaçage de l'écran
void effacer()
{
	SDL_FillRect(ecran,NULL,vert);
	texte = TTF_RenderText_Blended(fontMenu,"Jeu de la vie",fonteNoire);
	posTexte.x = WINWI/2-50;
	posTexte.y = 10;
	SDL_BlitSurface(texte,NULL,ecran,&posTexte);
}

//Rajout d'un pion sur le plateau
void rajout_pion(int x,int y)
{
	posPion.x = POSX_PLATEAU+20*x+1;
	posPion.y = POSY_PLATEAU+20*y+1;
	SDL_BlitSurface(pion,NULL,ecran,&posPion);
}


void rajout_pion_grille(int x,int y)
{
	posPion.x = POSX_GRILLE+20*x+1;
	posPion.y = POSY_GRILLE+20*y+1;
	SDL_BlitSurface(pion,NULL,ecran,&posPion);
}

//Rajout d'un pion sur le plateau
void rajout_pion_grille_eff(int x,int y)
{
	posPion.x = POSX_GRILLE+20*x+1;
	posPion.y = POSY_GRILLE+20*y+1;
	SDL_BlitSurface(pionEffac,NULL,ecran,&posPion);
}

//Affichage du plateau de jeu
void affichage_plateau(int plateau[TAILLE_X][TAILLE_Y])
{
	// Affichage du plateau de jeu
	SDL_Surface *board;
    board = IMG_Load("Images/plateau_vie.png");
    SDL_Rect posBoard;
    posBoard.x = POSX_PLATEAU;
    posBoard.y = POSY_PLATEAU;
    SDL_BlitSurface(board,NULL,ecran,&posBoard);
    int x,y;
    for(y=0;y!=TAILLE_Y;y++){
		for(x=0;x!=TAILLE_X;x++){
				if(plateau[x][y]==1) rajout_pion(x,y);
		}
	}
			

}

void affichage_grille(int plateau[11][11])
{
	// Affichage du plateau de jeu
	SDL_Surface *board;
    board = IMG_Load("Images/grille.png");
    SDL_Rect posBoard;
    posBoard.x = POSX_GRILLE;
    posBoard.y = POSY_GRILLE;
    SDL_BlitSurface(board,NULL,ecran,&posBoard);
    int x,y;
    for(y=0;y!=11;y++){
		for(x=0;x!=11;x++){
			
			if(plateau[x][y]==1) rajout_pion_grille(x,y);
			if(plateau[x][y]==2) rajout_pion_grille_eff(x,y);
			if(x==5 && y==5) {
				posPion.x = POSX_GRILLE+20*x+1;
				posPion.y = POSY_GRILLE+20*y+1;
				SDL_BlitSurface(pionCentre,NULL,ecran,&posPion);
			}
		}
	}
}

//Affichage d'un texte dans un rectangle
void affichage_rectangle(char ch[],int x,int y)
{
	texte = TTF_RenderText_Blended(fontMenu,ch,fonteNoire);
    posRect.x = x;
    posRect.y = y;
    posTexte.x = x+5;
    posTexte.y = y+5;
    SDL_BlitSurface(rectangle,NULL,ecran,&posRect);
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
}

//Affichage d'un texte
void affichage_texte(char ch[],int x,int y)
{
	texte = TTF_RenderText_Blended(fontMenu,ch,fonteNoire);
    posTexte.x = x+5;
    posTexte.y = y+5;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
}

//affichage du mini historique
void affichage_mini_historique(int j,int l,int hist[3][130],int fin)
{
	char ch[50];
	if(fin!=0){ //partie finie
		if(fin==1) affichage_rectangle("Rouge a gagne",POSX_MINI_TOUR,POSY_MINI_TOUR);
		if(fin==2) affichage_rectangle("Bleu a gagne",POSX_MINI_TOUR,POSY_MINI_TOUR);
		affichage_rectangle("Felicitation",POSX_MINI_HISTORIQUE,POSY_MINI_HISTORIQUE);
	}else{
		if(j==1)affichage_rectangle("Au rouge de jouer",POSX_MINI_TOUR,POSY_MINI_TOUR);
		else affichage_rectangle("Au bleu de jouer",POSX_MINI_TOUR,POSY_MINI_TOUR);
		if(l>0){ //au moins un coup a été jouer
			if(hist[0][l-1]==1) sprintf(ch,"Rouge a joue en %c %d",hist[1][l-1]+'A'-1,hist[2][l-1]);
			else sprintf(ch,"Bleu a joue en %c %d",hist[1][l-1]+'A'-1,hist[2][l-1]);
			affichage_rectangle(ch,POSX_MINI_HISTORIQUE,POSY_MINI_HISTORIQUE);
		}
	}
}



//Affichage des infos diverses
void affiche_info()
{
	texte = TTF_RenderText_Blended(fontMenu,"Bonjour, voici notre projet Logiciel de L2",fonteNoire);
    posTexte.x = 100; posTexte.y = 50;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    texte = TTF_RenderText_Blended(fontMenu,"Il est le fruit de la collaboration de:",fonteNoire);
    posTexte.x = 50; posTexte.y = 100;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    texte = TTF_RenderText_Blended(fontMenu,"-Mezieres Pierre",fonteNoire);
    posTexte.x = 50; posTexte.y = 150;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    texte = TTF_RenderText_Blended(fontMenu,"-Pelissier Nicolas",fonteNoire);
    posTexte.x = 50; posTexte.y = 200;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    texte = TTF_RenderText_Blended(fontMenu,"-Pujol Sylvain",fonteNoire);
    posTexte.x = 50; posTexte.y = 250;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    texte = TTF_RenderText_Blended(fontMenu,"Amusez-vous bien sur le HEX !",fonteNoire);
    posTexte.x = 50; posTexte.y = 300;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    texte = TTF_RenderText_Blended(fontMenu,"(Cliquez n'importe ou pour revenir au menu)",fonteNoire);
    posTexte.x = 150; posTexte.y = 520;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
}

//Affichage "alerte"
void affichage_alerte(char ch[],char ch2[],char ch3[])
{
	texte = TTF_RenderText_Blended(fontMenu,ch,fonteNoire);
    posTexte.x = 150; posTexte.y = 250;
    SDL_BlitSurface(texte,NULL,ecran,&posTexte);
    affichage_rectangle(ch2,POSX_CONFIRMER_ALERTE,POSY_CONFIRMER_ALERTE);
    affichage_rectangle(ch3,POSX_ANNULER_ALERTE,POSY_ANNULER_ALERTE);
}

void affichage_menu(int play,int vit,int cad)
{

    if(play) affichage_rectangle("PLAY",POSX_PLAY,POSY_PLAY);
    else affichage_rectangle("PAUSE",POSX_PLAY,POSY_PLAY);
    switch(vit)
    {
		case 1: affichage_rectangle("x1",POSX_VIT,POSY_VIT); break;
		case 2: affichage_rectangle("x2",POSX_VIT,POSY_VIT); break;
		case 3: affichage_rectangle("x3",POSX_VIT,POSY_VIT); break;
		case 4: affichage_rectangle("x4",POSX_VIT,POSY_VIT); break;
		default: break;
	}
    if(cad) affichage_rectangle("BLOQUER",POSX_CAD,POSY_CAD);
    else affichage_rectangle("NON BLOQUER",POSX_CAD,POSY_CAD);
    affichage_rectangle("QUITTER",POSX_QUITTER,POSY_QUITTER);
    affichage_rectangle("EFFACER",POSX_EFFACER,POSY_EFFACER);
    affichage_rectangle("EFFACER GRILLE",POSX_EFFAC,POSY_EFFAC);
}

//Affichage de l'historique
void affichage_historique(int hist[3][130],int length)
{
	effacer();
	affichage_texte("Les dix derniers coup:",250,90);
	char ch[50];
	int i,fin;
	if(length<11) fin=length+1;
	else fin=11;
	for(i=1;i!=fin;i++){
		if(hist[0][length-i]==1) strcpy(ch,"Rouge en ");
		else strcpy(ch,"Bleu en ");
		sprintf(ch,"%s %c %d",ch,hist[1][length-i]+'A'-1,hist[2][length-i]);
		affichage_rectangle(ch,250,100+40*i);
	}
	affichage_texte("(Cliquez n'importe ou pour quitter)",200,550);
}



//Reconnassance de clic "partiel"
int reco_clic(int x,int y,int x2,int y2)
{
	if(x>=x2 && y>=y2 && x<=x2+RECWI && y<=y2+RECHI) return 1;	
	return 0;
}

//Reconnaissance de clic "complet"
int reco_clic_complet(int x,int y,int x2, int y2, int x3, int y3)
{
	if(x>=x2 && y>=y2 && x<=x2+x3 && y<=y2+y3) return 1;	
	return 0;
}

// renvoie 0=rien;2=NouvellePartie;1=Jouer;3=Charger;4=Quitter;5=info diverses
int menu_principal(int x,int y)
{
	if(reco_clic(x,y,POSX_JOUER,POSY_JOUER)) return 1;
	if(reco_clic(x,y,POSX_NOUVELLE_PARTIE,POSY_NOUVELLE_PARTIE)) return 2;		
	if(reco_clic(x,y,POSX_CHARGER,POSY_CHARGER)) return 3;
	if(reco_clic(x,y,POSX_QUITTER,POSY_QUITTER)) return 4;
	if(reco_clic(x,y,POSX_INFO,POSY_INFO)) return 5;
	return 0;
}

// Renvoie 0=rien;1=HvsH;2=HvsIA1;3=HvsIA2;4=Retour;5=IAvsIA
int menu_jeu(int x,int y)
{
	if(reco_clic(x,y,POSX_HVSH,POSY_HVSH)) return 1;		
	if(reco_clic(x,y,POSX_HVSIA1,POSY_HVSIA1)) return 2;
	if(reco_clic(x,y,POSX_HVSIA2,POSY_HVSIA2)) return 3;
	if(reco_clic(x,y,POSX_RETOUR,POSY_RETOUR)) return 4;
	if(reco_clic(x,y,POSX_IAVSIA,POSY_IAVSIA)) return 5;
	return 0;
}

// Renvoie 0=rien;1=Slot1;2=Slot2;3=Slot3;4=Slot4;5=Confirmer;6=retour;
int menu_chargement(int x,int y,int *confirmer)
{
	if(reco_clic(x,y,POSX_CHARGER_1,POSY_CHARGER_1)) {*confirmer=1;return 1;}
	if(reco_clic(x,y,POSX_CHARGER_2,POSY_CHARGER_2)) {*confirmer=1;return 2;}
	if(reco_clic(x,y,POSX_CHARGER_3,POSY_CHARGER_3)) {*confirmer=1;return 3;}
	if(reco_clic(x,y,POSX_CHARGER_4,POSY_CHARGER_4)) {*confirmer=1;return 4;}
	if(*confirmer && reco_clic(x,y,POSX_CONFIRMER,POSY_CONFIRMER)) {*confirmer=0;return 5;}
	if(reco_clic(x,y,POSX_RETOUR_CHAR,POSY_RETOUR_CHAR)) {*confirmer=0;return 6;}
	return 0;
}

//Renvoie 0=rien;1=Sauver;2=Undo;3=Historique;4=retour;5=clic_plateau;6=faire jouer l'IA
int menu_en_jeu(int x,int y,int IA)
{
	if(reco_clic(x,y,POSX_SAUVER,POSY_SAUVER)) return 1;
	if(reco_clic(x,y,POSX_UNDO,POSY_UNDO)) return 2;
	if(reco_clic(x,y,POSX_HISTORIQUE,POSY_HISTORIQUE)) return 3;
	if(reco_clic(x,y,POSX_RETOUR_JEU,POSY_RETROU_JEU)) return 4;
	if(reco_clic_complet(x,y,POSX_PLATEAU,POSY_PLATEAU,PLATEAUWI,PLATEAUHI)) return 5;
	if(IA!=1 && reco_clic_complet(x,y,POSX_JOUER_IA,POSY_JOUER_IA,WIDT_JOUER_IA,HIGH_JOUER_IA)) return 6;
	return 0;
}

//Renvoie 0=rien;1=IA1vsIA1;2=IA1vsIA2;3=IA2vsIA2
int menu_IAvsIA(int x,int y)
{
	if(reco_clic(x,y,POSX_IA1VSIA1,POSY_IAvsIA)) return 1;
	if(reco_clic(x,y,POSX_IA1VSIA2,POSY_IAvsIA)) return 2;
	if(reco_clic(x,y,POSX_IA2VSIA2,POSY_IAvsIA)) return 3;
	return 0;
}

//Renvoie 1=slotA,2=slotB,3=slotC,4=slotD,5=confirmer,6=retour
int menu_sauvegarde(int x,int y,int *confirmer)
{
	if(reco_clic(x,y,POSX_CHARGER_1,POSY_CHARGER_1)) {*confirmer=1;return 1;}
	if(reco_clic(x,y,POSX_CHARGER_2,POSY_CHARGER_2)) {*confirmer=1;return 2;}
	if(reco_clic(x,y,POSX_CHARGER_3,POSY_CHARGER_3)) {*confirmer=1;return 3;}
	if(reco_clic(x,y,POSX_CHARGER_4,POSY_CHARGER_4)) {*confirmer=1;return 4;}
	if(*confirmer && reco_clic(x,y,POSX_CONFIRMER,POSY_CONFIRMER)) {*confirmer=0;return 5;}
	if(reco_clic(x,y,POSX_RETOUR_CHAR,POSY_RETOUR_CHAR)) {*confirmer=0;return 6;}
	return 0;
}

//Renvoi 0=rien;1=confirmer;2=annuler
int alerte(int x,int y)
{
	if(reco_clic(x,y,POSX_CONFIRMER_ALERTE,POSY_CONFIRMER_ALERTE)) return 1;
	if(reco_clic(x,y,POSX_ANNULER_ALERTE,POSY_ANNULER_ALERTE)) return 2;
	return 0;
}
