

/**********************************************************************/
/** Spécification des procédures et fonctions données dans le header **/
/**********************************************************************/

#include "jeu.h"
#include <stdbool.h>
#include <stdio.h>


int pion_valide(int x,int y)
{
	if(x>=0 && y>=0 && x<TAILLE_X && y<TAILLE_Y) return 1;
	return 0;
}

int compter_pion(int plateau[TAILLE_X][TAILLE_Y],int x,int y)
{
	int i,j;
	int comp=0;
	for(i=-1;i!=2;i++){
		for(j=-1;j!=2;j++){
			if((i!=0 || j!=0) && pion_valide(x+i,y+j)){
				if(plateau[x+i][y+j]) comp++;
			}
		}
	}
	return comp;
}

void jouer(int plateau[TAILLE_X][TAILLE_Y])
{
	int x,y;
	int pla[TAILLE_X][TAILLE_Y];
	int comp;
	for(x=0;x!=TAILLE_X;x++){
		for(y=0;y!=TAILLE_Y;y++){
			comp=compter_pion(plateau,x,y);
			if(comp==3) pla[x][y]=1;
			if(comp<=1 || comp>3) pla[x][y]=0;
			if(comp==2) pla[x][y]=plateau[x][y];
				
		}
	}
	for(x=0;x!=TAILLE_X;x++){
		for(y=0;y!=TAILLE_Y;y++){
			plateau[x][y]=pla[x][y];
				
		}
	}
}
